from tkinter import *
import random
import math

import ocean


class Ihm:
    AGENT_LENGTH = 10

    def __init__(self, environment):
        self.environment = environment  # l’océan dans notre cas
        fen = Tk()
        self.canvas = Canvas(fen, width=self.environment.max_width, height=self.environment.max_height, bg='white', bd=0)
        self.canvas.pack()
        self.fen = fen
        self.canvas.bind_all("a", self.add_fish)  # attacher une touche
        self.canvas.bind("<Button-3>", self.add_fish)  # attacher la souris
        self.fen.after(100, self.update)
        self.fen.mainloop()

    def add_fish(self, event):
        self.environment.add_fish(event.x, event.y, random.random() * 2 * math.pi)

    def draw_fish(self, _fishAgent):
        x = _fishAgent.x
        y = _fishAgent.y
        x2 = _fishAgent.x - _fishAgent.vx * Ihm.AGENT_LENGTH
        y2 = _fishAgent.y - _fishAgent.vy * Ihm.AGENT_LENGTH
        self.canvas.create_line(x,y, x2, y2, fill='blue')

    def draw(self):
        self.canvas.delete(ALL)
        for fish in self.environment.fishs:
            self.draw_fish(fish)

    def update(self):
        self.environment.update()
        self.draw()
        self.fen.after(25, self.update)

ocean = ocean.Ocean(100, 500, 500)
ihm = Ihm(ocean)
