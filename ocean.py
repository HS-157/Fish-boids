import math
import random
import fish_agent

class Ocean():

    def __init__(self, _nbFishs, _maxWidth, _maxHeight):
        self.nb_fishs = _nbFishs
        self.max_width = _maxWidth
        self.max_height = _maxHeight
        self.margin = _maxWidth * 10 / 100
        self.fishs = []
        self.obstacles = []
        for fish in range(self.nb_fishs):
            _x=self.max_width * random.random()
            _y=self.max_height * random.random()
            _direction=2 * math.pi * random.random()
            self.add_fish(_x=_x, _y=_y, _direction=_direction)

    def add_fish(self, _x, _y, _direction):
        self.fishs.append(fish_agent.FishAgent(_x=_x, _y=_y, _direction=_direction))

    def update(self):
        for fish in self.fishs:
            fish.update(self.fishs, self.obstacles, self.max_width, self.max_height, self.margin)
