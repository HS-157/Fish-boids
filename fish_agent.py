from world_object import WorldObject
import math

class FishAgent(WorldObject):

    STEP = 2.0
    DISTANCE_MIN = 10.0
    DISTANCE_MAX = 30.0
    SQUARE_DISTANCE_MIN = 100.0
    SQUARE_DISTANCE_MAX = 900.0
    WALL_AVOID = 0.3

    def __init__(self, _x, _y, _direction):
        self.direction = _direction
        self.vx = math.cos(self.direction)
        self.vy = math.sin(self.direction)
        super().__init__(_x, _y)

    def update_position(self):
        self.x += self.vx * FishAgent.STEP
        self.y += self.vy * FishAgent.STEP

    def normalise(self):
        norm = math.sqrt(self.vx ** 2 + self.vy ** 2)
        self.vx /= norm
        self.vy /= norm

    def near(self, _fishAgent):
        square_distance_to = self.square_distance_to(_fishAgent)
        if FishAgent.DSQUARE_ISTANCE_MIN <= square_distance_to <= SQUARE_DISTANCE_MIN:
            return True
        else:
            return False

    def update(self, _fishs, _obstacles, _maxWidth, _maxHeight, _margin):
        self.avoid_fishs(_fishs)
        self.avoid_wall(0 + _margin, _maxWidth - _margin, 0 + _margin, _maxHeight - _margin)
        self.update_position()

    def avoid_wall(self, _wallXMin, _wallXMax, _wallYMin, _wallYMax):
        if self.x < _wallXMin:
            self.vx += FishAgent.WALL_AVOID
        elif self.x > _wallXMax:
            self.vx -= FishAgent.WALL_AVOID
        elif self.y < _wallYMin:
            self.vy += FishAgent.WALL_AVOID
        elif self.y > _wallYMax:
            self.vy -= FishAgent.WALL_AVOID
        else:
            pass
        self.normalise()

    def avoid_fishs(self, _fishs):
        for fish in _fishs:
            if fish is self:
                continue

            square_distance = self.square_distance_to(fish)

            if square_distance < FishAgent.SQUARE_DISTANCE_MIN:
                x = fish.x - self.x
                y = fish.y - self.y
                norm_fish = math.sqrt(x ** 2 + y ** 2)
                x = x / norm_fish
                y = y / norm_fish
                self.vx -= x
                self.vy -= y
                self.normalise


