import math

class WorldObject():
    
    def __init__(self, _x, _y):
        self.x = _x
        self.y = _y

    def distance_to(self, world_object):
        return math.sqrt(square_distance_to(world_object))

    def square_distance_to(self, world_object):
        return (world_object.x - self.x) ** 2 + (world_object.y - self.y) ** 2
